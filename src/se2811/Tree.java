package se2811;

import java.util.Iterator;
import java.util.Map;

/**
 * Red-Black binary search tree, sorted by the value of the key.
 *
 * @param <K> Key type
 * @param <V> Value type
 */
class Tree<K extends Comparable<K>, V> implements Iterable<Map.Entry<K, V>> {
    private Node<K, V> root;

    /**
     * Check if a node is red. This is just a wrapper for node.isRed() that handles nulls.
     *
     * @param node The node to check.
     * @return True if it's red, false if it's black or null.
     */
    private boolean isRed(Node<K, V> node) {
        return node != null && node.isRed();
    }

    /**
     * Number of nodes in the subtree, or 0 if the tree is empty. This is just a wrapper for
     * node.getSize() that handles nulls.
     *
     * @param node
     * @return
     */
    private int size(Node<K, V> node) {
        return node == null ? 0 : node.getSize();
    }

    /**
     * @return Total nodes in the tree
     */
    public int size() {
        return size(root);
    }

    public boolean isEmpty() {
        return root == null;
    }

    public V get(K key) {
        if (key == null) {
            throw new IllegalArgumentException("key cannot be null");
        }
        return root.get(key);
    }

    boolean contains(K key) {
        return get(key) != null;
    }

    /**
     * Add a new node to the tree, or modify an existing node. Putting a null is the same as a
     * delete.
     *
     * @param key The key that we sort the tree by. This cannot be null.
     * @param val The value that's associated with the key.
     */
    public void put(K key, V val) {
        if (key == null) {
            throw new IllegalArgumentException("key cannot be null");
        }
        if (val == null) {
            // a better implementation would support null values, this one doesn't because it's
            // for a demo
            delete(key);
            return;
        }

        root = put(root, key, val);
        root.setIsRed(false);
    }

    private Node<K, V> put(Node<K, V> node, K key, V val) {
        if (node == null) {
            return new Node<>(key, val, true);
        }

        if (key.compareTo(node.getKey()) < 0) {
            node.setLeftChild(put(node.getLeftChild(), key, val));
        } else if (key.compareTo(node.getKey()) > 0) {
            node.setRightChild(put(node.getRightChild(), key, val));
        } else {
            node.setValue(val);
        }

        // fix-up tree after insert
        if (isRed(node.getRightChild()) && !isRed(node.getLeftChild())) {
            node = node.rotateLeft();
        }
        if (isRed(node.getLeftChild()) && isRed(node.getLeftChild().getLeftChild())) {
            node = node.rotateRight();
        }
        if (isRed(node.getLeftChild()) && isRed(node.getRightChild())) {
            node.flipColors();
        }
        node.setSize(size(node.getLeftChild()) + size(node.getRightChild()) + 1);

        return node;
    }

    private Node<K, V> deleteMin(Node<K, V> node) {
        if (node.getLeftChild() == null) {
            return null;
        }
        if (!isRed(node.getLeftChild()) && !isRed(node.getLeftChild().getLeftChild())) {
            node = moveRedLeft(node);
        }
        node.setLeftChild(deleteMin(node.getLeftChild()));
        return balance(node);
    }

    void delete(K key) {
        if (key == null) {
            // we could probably support null keys, but it would be super ugly
            throw new IllegalArgumentException("key cannot be null");
        }
        if (!contains(key)) {
            return;
        }

        // if both children of root are black, set root to red
        if (!isRed(root.getLeftChild()) && !isRed(root.getRightChild())) {
            root.setIsRed(true);
        }

        root = delete(root, key);
        if (!isEmpty()) {
            root.setIsRed(false);
        }
    }

    private Node<K, V> delete(Node<K, V> node, K key) {
        if (key.compareTo(node.getKey()) < 0) {
            if (!isRed(node.getLeftChild()) && !isRed(node.getLeftChild().getLeftChild())) {
                node = moveRedLeft(node);
            }
            node.setLeftChild(delete(node.getLeftChild(), key));
        } else {
            if (isRed(node.getLeftChild())) {
                node = node.rotateRight();
            }
            if (key.compareTo(node.getKey()) == 0 && (node.getRightChild() == null)) {
                return null;
            }
            if (!isRed(node.getRightChild()) && !isRed(node.getRightChild().getLeftChild())) {
                node = moveRedRight(node);
            }
            if (key.compareTo(node.getKey()) == 0) {
                Node<K, V> x = node.getRightChild().min();
                node.setKey(x.getKey());
                node.setValue(x.getValue());
                node.setRightChild(deleteMin(node.getRightChild()));
            } else {
                node.setRightChild(delete(node.getRightChild(), key));
            }
        }
        return balance(node);
    }

    private Node<K, V> moveRedLeft(Node<K, V> node) {
        node.flipColors();
        if (isRed(node.getRightChild().getLeftChild())) {
            node.setRightChild(node.getRightChild().rotateRight());
            node = node.rotateLeft();
            node.flipColors();
        }
        return node;
    }

    private Node<K, V> moveRedRight(Node<K, V> node) {
        node.flipColors();
        if (isRed(node.getLeftChild().getLeftChild())) {
            node = node.rotateRight();
            node.flipColors();
        }
        return node;
    }

    private Node<K, V> balance(Node<K, V> node) {
        if (isRed(node.getRightChild())) {
            node = node.rotateLeft();
        }
        if (isRed(node.getLeftChild()) && isRed(node.getLeftChild().getLeftChild())) {
            node = node.rotateRight();
        }
        if (isRed(node.getLeftChild()) && isRed(node.getRightChild())) {
            node.flipColors();
        }

        node.setSize(size(node.getLeftChild()) + size(node.getRightChild()) + 1);
        return node;
    }

    /**
     * @return An iterator that starts at root and returns all the values in the tree, sorted by
     * the key. The whole tree is sorted by the key and this does in-order traversal.
     */
    @Override
    public Iterator<Map.Entry<K, V>> iterator() {
        return new InOrderTreeIterator<>(root);
    }
}
