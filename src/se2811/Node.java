package se2811;

import java.util.Map;

class Node<K extends Comparable<K>, V> implements Map.Entry<K, V> {
    private Node<K, V> left = null;
    private Node<K, V> right = null;
    private K key;
    private V value;
    private boolean isRed;
    private int size;

    Node(K key, V value, boolean isRed) {
        this.key = key;
        this.value = value;
        this.isRed = isRed;
        this.size = 1;
    }

    V get(K key) {
        Node<K, V> node;
        int cmp = key.compareTo(this.key);
        if (cmp < 0) {
            node = left;
        } else if (cmp > 0) {
            node = right;
        } else {
            return value;
        }
        if (node == null) {
            return null;
        } else {
            return node.get(key);
        }
    }

    // flip the colors of a node and its two children
    void flipColors() {
        // node must have opposite color of its two children
        // assert (node != null) && (node.getLeftChild() != null) && (node.getRightChild() != null);
        // assert (!isRed(node) &&  isRed(node.getLeftChild()) &&  isRed(node.getRightChild()))
        //    || (isRed(node)  && !isRed(node.getLeftChild()) && !isRed(node.getRightChild()));
        isRed = !isRed;
        left.setIsRed(!left.isRed());
        right.setIsRed(!right.isRed());
    }

    // make a left-leaning link lean to the right and return the new top node in the tree
    Node<K, V> rotateRight() {
        // assert (node != null) && isRed(node.getLeftChild());
        Node<K, V> newTop = left;
        left = newTop.getRightChild();
        newTop.setRightChild(this);
        newTop.setIsRed(newTop.getRightChild().isRed());
        newTop.getRightChild().setIsRed(true);
        newTop.setSize(size);
        size = calculateSize();
        return newTop;
    }

    // make a right-leaning link lean to the left and return the new top node in the tree
    Node<K, V> rotateLeft() {
        // assert (node != null) && isRed(node.getRightChild());
        Node<K, V> newTop = right;
        right = newTop.getLeftChild();
        newTop.setLeftChild(this);
        newTop.setIsRed(newTop.getLeftChild().isRed());
        newTop.getLeftChild().setIsRed(true);
        newTop.setSize(getSize());
        size = calculateSize();
        return newTop;
    }

    // get the node that's furthest to the left
    Node<K, V> min() {
        return left == null ? this : left.min();
    }

    // get the node that's furthest to the right
    Node<K, V> max() {
        return right == null ? this : right.max();
    }

    /**
     * Used for updating the size parameter. Only recalculates 1-level deep, so as to avoid
     * traversing the whole subtree.
     *
     * @return
     */
    private int calculateSize() {
        return (left == null ? 0 : left.getSize()) + (right == null ? 0 : right.getSize()) + 1;
    }

    @Override
    public K getKey() {
        return key;
    }

    @Override
    public V getValue() {
        return value;
    }

    @Override
    public V setValue(V value) {
        return this.value = value;
    }

    void setKey(K key) {
        this.key = key;
    }

    Node<K, V> getLeftChild() {
        return left;
    }

    void setLeftChild(Node<K, V> node) {
        left = node;
    }

    Node<K, V> getRightChild() {
        return right;
    }

    void setRightChild(Node<K, V> node) {
        right = node;
    }

    boolean isRed() {
        return isRed;
    }

    void setIsRed(boolean isRed) {
        this.isRed = isRed;
    }

    int getSize() {
        return size;
    }

    void setSize(int size) {
        this.size = size;
    }
}