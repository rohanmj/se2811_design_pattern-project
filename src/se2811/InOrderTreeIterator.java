package se2811;

import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Stack;

public class InOrderTreeIterator<K extends Comparable<K>, V> implements Iterator<Map.Entry<K, V>> {
    // the last item on the stack is always the current node. the stack represents all the
    // parents that the current node is the child of. this stack will never be longer than the
    // max depth of the tree
    private Stack<Node<K, V>> parents;

    // this stack keeps track of the number of "children" that have been emitted for each node in
    // the parents stack. every element in this stack corresponds to one element (at the same
    // position) in the parents stack. a value of 0 means nothing has been emitted for the node,
    // 1 means the left side is done, 2 means the value of the node has been emitted. there is no
    // 3 value because we remove elements from the parents stack as soon as the right side has
    // been emitted.
    private Stack<Integer> progress;

    InOrderTreeIterator(Node<K, V> root) {
        parents = new Stack<>();
        progress = new Stack<>();
        if (root != null) {
            // null being passed in means that the tree is empty, so we have nothing to iterate over
            addToStack(root);
        }
    }

    @Override
    public boolean hasNext() {
        return !parents.empty();
    }

    @Override
    public Map.Entry<K, V> next() {
        return getNext();
    }

    private void addToStack(Node<K, V> node) {
        progress.push(0);
        parents.push(node);
        attemptIncrement();
    }

    private void incrementProgress() {
        if (progress.empty()) {
            // no more progress to increment
            return;
        }
        int nodeProgress = progress.pop();
        if (nodeProgress == 0) {
            // we're done with the left side
            progress.push(1);
        } else if (nodeProgress == 1) {
            // we're done with the value of the node itself
            progress.push(2);
        } else {
            // we're done with the right side, remove this node and increment the parent
            parents.pop();
            incrementProgress();
        }
        attemptIncrement();
    }

    private void attemptIncrement() {
        if (progress.empty()) {
            // no more progress to increment
            return;
        }

        int nodeProgress = progress.peek();
        Node<K, V> node = parents.peek();

        if (nodeProgress == 0) {
            if (node.getLeftChild() == null) {
                // left side is null, fix the progress counter
                incrementProgress();
            }
        } else if (nodeProgress == 2) {
            if (node.getRightChild() == null) {
                // right side is null, fix the progress counter
                incrementProgress();
            }
        }
    }

    private Map.Entry<K, V> getNext() {
        if (!hasNext()) {
            throw new NoSuchElementException("no more elements to iterate");
        }

        int nodeProgress = progress.peek();
        Node<K, V> node = parents.peek();

        if (nodeProgress == 1) {
            // we're ready to emit the current value of the node
            incrementProgress();
            return node;
        }

        if (nodeProgress == 0) {
            assert node.getLeftChild() != null;
            // add the left child to the stack and recurse into that
            addToStack(node.getLeftChild());
        } else if (nodeProgress == 2) {
            assert node.getRightChild() != null;
            // add the right child to the stack and recurse into that
            addToStack(node.getRightChild());
        }
        return getNext();
    }
}
