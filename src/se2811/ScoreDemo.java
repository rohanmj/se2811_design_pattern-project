package se2811;

import java.util.Iterator;
import java.util.Map.Entry;
import java.util.concurrent.ThreadLocalRandom;

class ScoreDemo {
    void run() {
        Tree<String, Integer> scores = new Tree<>();
        ThreadLocalRandom random = ThreadLocalRandom.current();
        for (int i = 1; i < 10; i++) {
            scores.put("Player " + i, random.nextInt(0, 100));
        }

        System.out.println("\n----- Run demo 1 -----\n");

        Iterator<Entry<String, Integer>> iter = scores.iterator();
        while (iter.hasNext()) {
            Entry<String, Integer> scoreEntry = iter.next();
            System.out.println("player name: " + scoreEntry.getKey());
            System.out.println("player score: " + scoreEntry.getValue());
        }
    }

    void run2() {
        Tree<Integer, String> scores = new Tree<>();
        ThreadLocalRandom random = ThreadLocalRandom.current();
        for (int i = 1; i < 10; i++) {
            scores.put(random.nextInt(0, 100), "Player " + i);
        }

        System.out.println("\n-----Run demo 2-----\n");

        for (Entry<Integer, String> scoreEntry : scores) {
            System.out.println("player name: " + scoreEntry.getValue());
            System.out.println("player score: " + scoreEntry.getKey());
        }
    }
}