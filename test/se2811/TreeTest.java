package se2811;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.Map;

class TreeTest {
    private Tree<String, Integer> tree;
    private Tree<Integer, String> tree2;

    @BeforeEach
    void before() {
        tree = new Tree<>();
        tree2 = new Tree<>();
    }

    private void fillTree() {
        // fill the tree with a bunch of random data
        int entries = (int) Math.floor(100 * Math.random());
        for (int i = 0; i < entries; i++) {
            tree.put("P" + (i * 100 * Math.random()), (int) Math.floor(i * 100 * Math.random()));
        }
    }

    @Test
    void size() {
        Assertions.assertEquals(0, tree.size());
        tree.put("mykey", 42);
        Assertions.assertEquals(1, tree.size());
        tree.put("mykey", 20);
        Assertions.assertEquals(1, tree.size());
        tree.put("mykey2", 30);
        tree.put("mykey3", 20);
        Assertions.assertEquals(3, tree.size());
    }

    @Test
    void isEmpty() {
        Assertions.assertTrue(tree.isEmpty());
        tree.put("mykey", 42);
        Assertions.assertFalse(tree.isEmpty());
    }

    @Test
    void get() {
        tree.put("mykey5", 20);
        tree.put("mykey2", 30);
        tree.put("mykey3", 20);
        tree.put("mykey2", 30);
        tree.put("mykey", 20);
        tree.put("mykey7", -20);
        tree.put("mykey9", null);
        Assertions.assertEquals(20, (int) tree.get("mykey3"));
        Assertions.assertEquals(-20, (int) tree.get("mykey7"));
        Assertions.assertEquals(null, tree.get("mykey9"));
    }

    @Test
    void contains() {
        fillTree();
        tree.put("P10", 30);
        Assertions.assertTrue(tree.contains("P10"));
        Assertions.assertFalse(tree.contains("S10"));
    }

    @Test
    void put() {
        tree.put("mykey5", 20);
        tree.put("mykey2", 30);
        tree.put("mykey3", 20);
        tree.put("mykey2", 30);
        tree.put("mykey", 20);
        tree.put("mykey7", -20);
        tree.put("mykey9", null);

        // putting a null is a delete
        Assertions.assertFalse(tree.contains("mykey9"));
    }

    @Test
    void delete() {
        tree.put("mykey5", 20);
        tree.put("mykey2", 30);
        tree.put("mykey3", 20);
        tree.put("mykey2", 30);
        tree.put("mykey", 20);
        tree.put("mykey7", -20);
        tree.put("mykey9", null);
        Assertions.assertTrue(tree.contains("mykey7"));
        tree.delete("mykey7");
        Assertions.assertFalse(tree.contains("mykey7"));
    }

    @Test
    void min() {
    }

    @Test
    void max() {
    }

    @Test
    void iterator() {
        for (Map.Entry<Integer, String> entry : tree2) {
            Assertions.fail("the loop shouldn't run at all if the tree is empty");
        }

        int maxTreeSize = 50;
        for (int size = 0; size < maxTreeSize; size++) {
            tree2 = new Tree<>();
            for (int i = 0; i < size; i++) {
                tree2.put(i, "" + i);
            }
            int expectedNumber = 0;
            for (Map.Entry<Integer, String> entry : tree2) {
                Assertions.assertEquals(expectedNumber, (int) entry.getKey());
                expectedNumber++;
            }
        }
    }

    @Test
    void iterator2() {
        tree2 = new Tree<>();
        for (int i = 0; i < 10; i++) {
            tree2.put(i, "" + i);
        }

        Iterator<Map.Entry<Integer, String>> iter = tree2.iterator();
        int expectedNumber = 0;
        while (iter.hasNext()) {
            Assertions.assertEquals(expectedNumber, (int) iter.next().getKey());
            expectedNumber++;
        }
    }

    @AfterEach
    void checkBalanced() throws NoSuchFieldException, NoSuchMethodException, IllegalAccessException,
                                InvocationTargetException {
        Field rootField = tree.getClass().getDeclaredField("root");
        rootField.setAccessible(true);
        Node<String, Integer> root = (Node<String, Integer>) rootField.get(tree);
        Method isRed = tree.getClass().getDeclaredMethod("isRed", Node.class);
        isRed.setAccessible(true);

        int black = 0; // number of black nodes from root to the left-most child of the tree
        Node<String, Integer> node = root;
        while (node != null) {
            if (!(Boolean) isRed.invoke(tree, node)) {
                black++;
            }
            node = node.getLeftChild();
        }
        Assertions.assertTrue(checkBalanced(root, black));
    }

    // make sure each path from the root to the bottom has the name number of blacks
    private boolean checkBalanced(Node<String, Integer> node, int black) {
        if (node == null) {
            return black == 0;
        }
        if (!node.isRed()) {
            black--;
        }
        return checkBalanced(node.getLeftChild(), black) &&
                checkBalanced(node.getRightChild(), black);
    }
}